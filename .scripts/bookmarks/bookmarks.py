#!/usr/bin/python

import json
import webbrowser
import subprocess

with open('/home/ramraj/.scripts/bookmarks/bookmarks.json') as f:
    bookmarks = json.load(f)['bookmarks']

options = '\n'.join([f'{bookmark["title"]}' for bookmark in bookmarks])

n = str(len(bookmarks))

proc = subprocess.Popen(
    ['rofi', '-dmenu', '-p', 'bookmarks', '-lines', n, '-i', '-width', '500'],
    stdin=subprocess.PIPE,
    stdout=subprocess.PIPE
)

proc.stdin.write((options).encode('utf-8'))
proc.stdin.close()

answer = proc.stdout.read().decode('utf-8')

matches = []

if answer:
    matches = list(filter(lambda x: x['title'].lower().startswith(answer.strip().lower()), bookmarks))

if matches:
    url = matches[0]['url']
    webbrowser.open(url)
