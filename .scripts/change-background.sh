#!/bin/bash

ans="/home/ramraj/Pictures/wallpapers/$(echo -e "$(ls ~/Pictures/wallpapers -I wallpaper.jpg)" | rofi -dmenu -i -p "Background")"
echo "Copying $ans to ~/Pictures/wallpapers/wallpaper.jpg"
cp "$ans" ~/Pictures/wallpapers/wallpaper.jpg
feh --bg-fill ~/Pictures/wallpapers/wallpaper.jpg
